## Quadratic equation solver
This is a practice package _quadratic-equation-andpos_. A function to solve quadratic equation:

_from quadratic_equation.qemath import solve_quadratic_equation_

_solve_quadratic_equation(a, b, c)_

Returns a tuple of two or one result, and if no results, then None. Three example results: _(-2.0, -0.5)_, _(-1.0,)_, _None_.
<br />If tuple has two results, then lesser value is first.


### To create a package [(tutorial)](https://packaging.python.org/tutorials/packaging-projects/)
This section is for future reference.

#### Create project structure
Check tutorial link for the contents of files.
* _quadratic_equation/\_\_init\_\_.py_ (and actual package files)
* _setup.py_
* _LICENSE_
* _README.md_

#### Install latest versions of _setuptools_, _wheel_ and _twine_
* _pip install --user --upgrade setuptools wheel twine_

#### Generate distribution archives
* _setup.py sdist bdist_wheel_

#### Upload the distribution archives
Command for test and real repository.
* _twine upload --skip-existing --repository-url https://test.pypi.org/legacy/ dist/*_
* _twine upload --skip-existing dist/*_
<br />__Username:__ \_\__token___
<br />__Password:__ _pypi-..._

#### Install the package
Command for test and real repository.
* _pip install -i https://test.pypi.org/simple/ --no-deps quadratic-equation-andpos_
* _pip install quadratic-equation-andpos_