import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="quadratic-equation-andpos",
    version="0.0.4",
    author="Andreas",
    author_email="this-is-fake@mail",
    description="Quadratic equation solver",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/anpos/homework1",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
