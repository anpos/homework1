"""Math for solving quadratic equation."""

from math import sqrt
from typing import Union


def solve_quadratic_equation(a: Union[int, float], b: Union[int, float], c: Union[int, float]) -> Union[None, tuple]:
    """
    Solve the quadratic equation.

    :param a: variable for quadratic equation
    :param b: variable for quadratic equation
    :param c: variable for quadratic equation

    :return: x1, x2 as a tuple, or x as a tuple, or simply None
    """
    if a == 0:
        if b == 0:
            return None
        if c == 0:
            return 0.0,
        return -c / b,
    if b == 0 and c == 0:
        return 0.0,
    d = b * b - a * c * 4
    if d < 0:
        return None
    elif d == 0:
        return -b / (a * 2),
    sqrt_d = sqrt(d)
    x1 = (-b - sqrt_d) / (a * 2)
    x2 = (-b + sqrt_d) / (a * 2)
    r = (x1, x2) if x1 < x2 else (x2, x1)
    return r
